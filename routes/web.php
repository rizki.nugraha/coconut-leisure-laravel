<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
});

Route::get('/leisure', function() {
    return view('leisure.index');
})->name('leisure');

Route::get('/about-us', function () {
    return view('about.index');
})->name('about');

Route::get('/how-we-work', function () {
    return view('howWeWork.index');
})->name('how-we-work');

Route::get('/mice', function () {
    return view('mice.index');
})->name('mice');

Route::get('/faq', function() {
    return view('faq.index');
})->name('faq');
