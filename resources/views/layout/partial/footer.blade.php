<footer class="cl-footer__base-container">
    <div class="cl-footer__content-container">
        <div class="cl-footer__media-container">
            <a href="/" class="cl-footer__logo-container">
                <img src="{{ asset('images/logo/logo-coconut-leisure-white.svg') }}">
            </a>
            <span class="cl-footer__copy-text">&copy; Copyright {{ now()->year }} Coconutleisutre. All Rights
                Reserved.</span>
            <div class="cl-footer__social-logo">
                <a href="">
                    <span class="icon-facebook"></span>
                </a>
                <a href="">
                    <img src="{{ asset('images/logo/instagram-solid.svg') }}">
                </a>
                <a href="">
                    <span class="icon-twitter"></span>
                </a>
                <a href="">
                    <span class="icon-youtube"></span>
                </a>
            </div>
        </div>
        <div class="cl-footer__nav-container">
            <nav>
                <a href="{{ route('about') }}">About Coconutleisure</a>
                <a href="{{ route('leisure') }}">Leisure</a>
                <a href="">Why Coconutleisure</a>
                <a href="{{ route('mice') }}">Mice</a>
                <a href="{{ route('how-we-work') }}">How We Work</a>
                <a href="">FAQ</a>
                <a href="">Contact</a>
            </nav>
        </div>
    </div>
</footer>
