<header class="cl-header__base-container shadow">
    <div class="cl-header__content-container">
        <div class="cl-header__hamburger-container">
            <button id="hamburgerBtn" class="cl-header__hamburger-btn btn">
                <span class="cl-header__hamburger-line"></span>
            </button>
        </div>
        <div class="cl-header__logo-container">
            <a href="/">
                <img class="cl-header__logo" src="{{ asset('images/logo/coconut-leisure-logo.svg') }}">
            </a>
        </div>
        <div class="cl-header__menu-container">
            <nav class="cl-header__nav-menu">
                <a href="{{ route('leisure') }}">leisure</a>
                <a href="{{ route('mice') }}">mice</a>
                <a href="{{ route('about') }}">about us</a>
                <a href="{{ route('how-we-work') }}">how we work</a>
                <a href="{{ route('faq') }}">faq</a>
            </nav>
            <div>
                <a class="cl-header__whatsapp-btn shadow" href="/">
                    <span class="icon-whatsapp me-lg-2"></span>
                    <span class="cl-header__whatsapp-btn--text">Whatsapp!</span>
                </a>
            </div>
        </div>
    </div>
</header>
