@extends('layout.app')

@section('content')
<section class="cl-home__hero-banner">
  <img class="cl-home__hero-banner--img" src="{{ asset('/images/illustration/hero-banner.png') }}">
  <div class="cl-home__hero-banner--text">
    <p class="mb-1">Here, Meaningfull Travel Awaits.</p>
    <h1>Endless Experiences, Awaits.</h1>
  </div>

  {{-- filter box --}}
  <div class="cl-home__hero-banner--filter-box">
    <form>
      <div class="row">
        <div class="col">
          <label class="mb-2" for="where">Where To Go?</label>
          <input type="text" class="form-control" placeholder="First name" name="where">
        </div>
        <div class="col">
          <label class="mb-2" for="size">Your Group Size?</label>
          <input type="text" class="form-control" placeholder="Last name" name="size">
        </div>
        <div class="col">
          <label class="mb-2" for="days">How Many Days?</label>
          <input type="text" class="form-control" placeholder="Last name" name="days">
        </div>
        <div class="col-1"></div>
      </div>
    </form>
  </div>
</section>
<section class="cl-home__carding-des">

</section>
@endsection